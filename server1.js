"use strict";

const Hapi = require("hapi");
const Inert = require("inert");
const Vision = require("vision");
const auth = require("./middleware/auth");
const Path = require("path");
const dotenv = require("dotenv");
dotenv.config();
const HapiSwagger = require("hapi-swagger");
const sequelize = require("./models/index");
sequelize.sync({});

// Create a server with a host and port
const server = Hapi.server({
  host: "localhost",
  port: 8000,
});

// Start the server
async function start() {
  try {
    const swaggerOptions = {
      info: {
        title: "Test API Documentation",
        version: "1.0.0.0",
      },
    };
    server.auth.scheme("custom", auth.register);
    server.auth.strategy("default", "custom");
    server.auth.default("default");

    await server.register([
      Inert,
      Vision,

      {
        plugin: HapiSwagger,
        options: swaggerOptions,
      },
    ]);
    await server.register({
      plugin: require("hapi-dev-errors"),
      options: {
        showErrors: process.env.NODE_ENV !== "production",
        // useYouch: true,
      },
    });

    await server.register([
      require("./Routes/todoRoutes"),
      require("./Routes/someOtherRoutes"),
    ]);

    await server.start();
  } catch (err) {
    console.log(err);
    process.exit(1);
  }

  console.log("Server running at:", server.info.uri);
}

start();
