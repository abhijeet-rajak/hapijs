const {
  register,
  login,
  update_task,
  delete_task,
  add_task,
  fetchXls,
} = require("../Controller/todoController");
// require("../db");

const Joi = require("joi");
const Response = require("../util/responseHandler");
const todoRoutes = {
  name: "routes",

  register: function (server, option) {
    const routes = [
      {
        method: "GET",
        path: "/test",
        config: {
          auth: false,
          description: "This test route",
          notes: "no aguments are passed",
          tags: ["api"],
        },
        handler: function (request, h) {
          return new Response("hello", 201);
        },
      },
      {
        method: "POST",
        path: "/register",
        config: {
          auth: false,
          description: "This is register route",
          notes: "send email name and password",
          tags: ["api"],
          handler: register,
          validate: {
            payload: Joi.object({
              name: Joi.string().min(1).max(30),
              email: Joi.string().email(),
              password: Joi.string(),
            }),
          },
        },
      },
      {
        method: "POST",
        path: "/login",
        config: {
          auth: false,
          description: "This Login route",
          notes: "payload require email and password",
          tags: ["api"],
          handler: login,
          validate: {
            payload: Joi.object({
              email: Joi.string().email(),
              password: Joi.string(),
            }),
          },
        },
      },
      {
        method: "POST",
        path: "/create-task",
        config: {
          description: "This route is used for creating todo",
          notes: "no aguments are passed",
          tags: ["api"],
          handler: add_task,
          validate: {
            payload: Joi.object({
              todo_task: Joi.string().required(),
            }),
            headers: Joi.object({
              Authorization: Joi.string(),
            }).options({
              allowUnknown: true,
            }),
          },
        },
      },
      {
        method: "PATCH",
        path: "/update-task/{id}",
        config: {
          description: "This route is for update task",
          notes: "no aguments are passed",
          tags: ["api"],
          handler: update_task,
          validate: {
            params: Joi.object({
              id: Joi.number().min(1).max(30),
            }),
            payload: Joi.object({
              todo_task: Joi.string(),
              is_completed: Joi.boolean(),
            }),
            headers: Joi.object({
              Authorization: Joi.string(),
            }).options({
              allowUnknown: true,
            }),
          },
        },
      },
      {
        method: "DELETE",
        path: "/delete-task/{id}",

        config: {
          description: "This route is for delete task",

          notes: "no aguments are passed",
          tags: ["api"],
          handler: delete_task,
          validate: {
            params: Joi.object({
              id: Joi.number().min(1).max(30),
            }),
            headers: Joi.object({
              Authorization: Joi.string(),
            }).options({
              allowUnknown: true,
            }),
          },
        },
      },
      {
        method: "GET",
        path: "/fetch-xls",
        config: {
          description: "This route is used for fetching xls file",
          notes: "no aguments are passed",
          tags: ["api"],
          auth: false,
          handler: fetchXls,
        },
      },
    ];
    server.route(routes);
  },
};

todoRoutes.register.attributes = {
  name: "todo-routes",
  vesion: "1.0.0",
};

module.exports = todoRoutes;
