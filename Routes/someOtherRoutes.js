const { Router } = require("express");
const Joi = require("joi");
const AppError = require("../util/errorHandler");

const router = Router();

const someOtherRoutes = {
  name: "some other route",

  register: function (server, option) {
    const routes = [
      {
        method: "GET",
        path: "/some-route",
        config: {
          auth: false,
          description: "This test route",
          notes: "no aguments are passed",
          tags: ["api"],
          handler: function (response, reply) {
            try {
              console.lo("hello");
              //   return h.response("hello some other route").code(201);
              //   return h.response(Boom.badError("hello"));
              throw new Error("hello error");
            } catch (err) {
              return new AppError("error have happened", 400);
              console.log(err.message);
            }
          },
        },
      },
    ];
    server.route(routes);
  },
};

someOtherRoutes.register.attributes = {
  name: "some-other-routes",
  vesion: "1.0.0",
};

module.exports = someOtherRoutes;
