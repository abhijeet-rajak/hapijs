const { Sequelize, Model, DataTypes } = require("sequelize");

// const sequelize = require("../db");
const sequelize = require("../models/index");

class Todo extends Model {}

const todoSchema = {
  todo_id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  user_id: {
    type: DataTypes.UUID,
  },
  todo_task: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  is_completed: {
    type: DataTypes.BOOLEAN,
  },
};

Todo.init(todoSchema, {
  sequelize,
  tableName: "todo",
});

module.exports = Todo;
