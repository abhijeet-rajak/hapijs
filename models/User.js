const { Model, DataTypes } = require("sequelize");
const Todo = require("./Todo");
// const sequelize = require("../db");
const sequelize = require("../models/index");

const { hash } = require("bcrypt");

class User extends Model {}

const userSchema = {
  user_id: {
    type: DataTypes.UUID,
    primaryKey: true,
  },
  name: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  email: {
    type: DataTypes.STRING,
    isEmail: true,
    allowNull: false,
    unique: true,
  },
  password: {
    type: DataTypes.STRING,
    allowNull: false,
  },
};

User.init(userSchema, {
  sequelize,
  tableName: "user",
});

User.hasMany(Todo, {
  foreignKey: "user_id",
});
Todo.belongsTo(User, {
  foreignKey: "user_id",
});
User.beforeCreate(async (models, option) => {
  const hashPassword = await hash(models.getDataValue("password"), 10);
  models.setDataValue("password", hashPassword);
});

module.exports = User;
