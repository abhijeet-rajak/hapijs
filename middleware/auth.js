const { verify } = require("jsonwebtoken");
const AppError = require("../util/errorHandler");
const Response = require("../util/responseHandler");
const auth = {
  name: "routes",
  register: (server, option, next) => {
    return {
      authenticate: function (request, h) {
        console.log(request.headers.authorization);
        try {
          const verifiedToken = verify(
            request.headers.authorization,
            process.env.SECRET_KEY
          );
          console.log(verifiedToken);
          request.id = verifiedToken.user_id;
          return h.continue;
        } catch (err) {
          console.log(err);
          // console.log(new AppError(err.message, err.name, 403));
          // return new AppError(err.message, err.name, 403);
          return h.response(err.message).code(403).takeover();
        }
      },
    };
  },
};

module.exports = auth;
