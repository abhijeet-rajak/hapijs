const Hapi = require("hapi");
const sequelize = require("./models/index");
const env = require("dotenv");
const auth = require("./middleware/auth");
const Inert = require("inert");
const Vision = require("vision");
const HapiSwagger = require("hapi-swagger");
const Pack = require("./package.json");
env.config();
sequelize.sync({});

const init = async () => {
  try {
    const server = new Hapi.Server({
      port: 3000,
      host: "localhost",
    });

    const func = async (request, username, password) => {
      try {
        console.log("hello world");
        // return true;
        return "helllloooooo";
      } catch (err) {
        console.log(err);
      }
    };

    const swaggerOptions = {
      name: "hello",
      info: {
        title: "Test API Documentation",
        version: Pack.version,
      },
    };
    // console.log("async function", require("@hapi/basic").plugin.register());
    // await server.register(require("hapi-auth-basic"));
    // server.auth.strategy("simple", "basic", { validate: func });
    // server.auth.default("simple");
    server.auth.scheme("custom", auth.register);
    server.auth.strategy("default", "custom");
    server.auth.default("default");

    // server.route({
    //   method: "GET",
    //   path: "/",
    //   config: {
    //     auth: "default",
    //     handler: function (request, h) {
    //       return "welcome";
    //     },
    //   },
    // });
    // await server.register(

    await server.register([
      Inert,
      Vision,
      {
        plugin: HapiSwagger,
        options: swaggerOptions,
      },
    ]);

    await server.register([require("./Routes/todoRoutes")]);
    await server.start();
    console.log("Server runnig on", server.info.uri);

    // global error handler
    // server.ext("onPreResponse", (request, h) => {
    //   if (request.response.name == "SequelizeUniqueConstraintError") {
    //     return h.response("email is already registerd").code(400);
    //   } else if (request.response.name === "DATANOTFOUND") {
    //     return h
    //       .response(request.response.message)
    //       .code(request.response.statusCode);
    //   } else if (request.response.name == "DataNotUpdated") {
    //     return h
    //       .response(request.response.message)
    //       .code(request.response.statusCode);
    //   } else if (
    //     request.response.statusCode >= 200 &&
    //     request.response.statusCode < 300
    //   ) {
    //     return h
    //       .response(request.response.source)
    //       .code(request.response.source.statusCode);
    //   } else if (request.response.name == "TokenExpiredError") {
    //     return h
    //       .response("Your token is expired")
    //       .code(request.response.statusCode);
    //   } else if (request.response.name == "JsonWebTokenError") {
    //     return h
    //       .response("Your token is malformed or tampered")
    //       .code(request.response.statusCode);
    //   } else {
    //     return h
    //       .response("Something went wrong !")
    //       .code(request.response.statusCode);
    //   }
    // });
  } catch (err) {
    console.log("server error", err);
  }
};

process.on("unhandledRejection", (err) => {
  console.log(err);
  process.exit(1);
});

init();
