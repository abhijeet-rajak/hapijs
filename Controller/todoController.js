const User = require("../models/User");
const Todo = require("../models/Todo");
const AppError = require("../util/errorHandler");
const Response = require("../util/responseHandler");
const xlsx = require("xlsx");
// const file = require("../files/file.xlsx");
const { compare } = require("bcrypt");
const { sign } = require("jsonwebtoken");
const uuid = require("uuid/v4");

const {
  createOne,
  getOne,
  updateModel,
  delete_todo,
} = require("../util/handleFactory");
module.exports = {
  register: async (request, h) => {
    try {
      let payload = {};
      payload = { ...request.payload, user_id: uuid() };

      const newUser = await createOne(User, payload);

      return new Response("success", 201);
    } catch (err) {
      if (err.name === "SequelizeUniqueConstraintError")
        return new AppError("email is already registered", err.name, 500);
    }
  },

  login: async (request, h) => {
    try {
      const { password, email } = request.payload;
      const user = await getOne(User, "email", email);
      const isPassword = await compare(password, user.getDataValue("password"));

      if (!isPassword) {
        throw new AppError("Not Found !", "InvalidEmailAndPassword", 404);
      }

      console.log(process.env.SECRET_KEY);
      const token = sign(
        { user_id: user.getDataValue("user_id") },
        process.env.SECRET_KEY,
        {
          expiresIn: 60 * 10,
        }
      );

      return new Response(
        `Welcome ${user.getDataValue("name")} to trello board`,
        200,
        { token }
      );
    } catch (err) {
      console.log(err);
      return new AppError(
        err.message == "Not Found !"
          ? "Invalid Email And Password"
          : err.message,
        err.name,
        err.statusCode || 500
      );
    }
  },

  add_task: async (request, h) => {
    try {
      console.log("in add route");
      let payload = { ...request.payload, user_id: request.id };

      const newTodo = await createOne(Todo, payload);

      return new Response("success", 201);
    } catch (err) {
      console.log(err);
      return new AppError(err.message, err.name, 500);
    }
  },
  update_task: async (request, h) => {
    try {
      await updateModel(
        Todo,
        { ...request.payload },
        { where: { todo_id: request.params.id } }
      );
      return new Response("success", 201);
    } catch (err) {
      console.log(err);
      return new AppError(err.message, err.name, err.statusCode || 500);
    }
  },
  delete_task: async (req, h) => {
    try {
      await delete_todo(Todo, { where: { todo_id: req.params.id } });
      return new Response("success", 201);
    } catch (err) {
      console.log(err);
      return new AppError(
        err.message == "Not Found !" ? "Todo Not Found !" : err.message,
        err.name,
        err.statusCode || 500
      );
    }
  },
  fetchXls: async (req, h) => {
    try {
      const wb = xlsx.readFile("./files/file.xlsx");

      let attributesToFilter = ["Payment Method", "Order Id", "Created Date"];
      let sheetName = wb.SheetNames;

      let data = {};
      sheetName.map((sn) => {
        data[sn] = xlsx.utils.sheet_to_json(wb.Sheets[sn]);

        // console.log(objKeys);
      });
      // const ws = wb.Sheets[sn];
      // data.map((d) => console.log(d));

      let filteredData = [];
      sheetName.map((sn) =>
        data[sn].filter((f) => {
          let obj = {};
          for (let k of attributesToFilter) {
            obj[k] = f[k];
          }
          filteredData.push(obj);
        })
      );

      return new Response("success", 201, { filteredData });
    } catch (err) {
      console.log(err);
      return new AppError(err.message, err.name, err.statusCode || 500);
    }
  },
};
