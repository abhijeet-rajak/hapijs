class Respnse {
  constructor(message, statusCode, ...rest) {
    this.statusCode = statusCode;
    this.message = message;

    if (rest.length > 0)
      rest.map((r, idx) =>
        Object.keys(r).map((key) => (this[key] = rest[idx][key]))
      );
  }
}

module.exports = Respnse;
