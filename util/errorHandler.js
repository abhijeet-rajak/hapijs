class AppError {
  constructor(message, name, statusCode) {
    // super(message);
    this.message = message;
    this.statusCode = statusCode;
    this.status = `${statusCode}`.startsWith("4") ? "fail" : "error";
    this.isOperational = true;
    this.name = name;
  }
}

module.exports = AppError;
