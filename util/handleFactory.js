const AppError = require("../util/errorHandler");
const Response = require("../util/responseHandler");
const uuid = require("uuid/v4");
module.exports = {
  createOne: async (Model, payload) => {
    try {
      let doc;
      doc = await Model.create({ ...payload });
      return new Response("sucess", 200, doc.dataValues);
    } catch (err) {
      console.log("MESSAGE", err.message);
      throw new AppError(err.message, err.name, 400);
    }
  },

  getOne: async (Model, filterBy, value) => {
    try {
      let doc;
      doc = await Model.findOne({
        where: {
          [filterBy]: value,
        },
      });
      if (!doc) throw new AppError("Not Found !", "DATANOTFOUND", 404);
      return doc;
    } catch (err) {
      console.log("MESSAGE", err);
      throw new AppError(err.message, err.name, err.statusCode || 500);
    }
  },
  updateModel: async (Model, set, condition) => {
    console.log(set);
    console.log(condition);
    try {
      const updatedDoc = await Model.update({ ...set }, condition);
      console.log(updatedDoc);
      if (!updatedDoc[0]) {
        throw new AppError("Not Updated !", "DataNotUpdated", 404);
      }
      return updatedDoc[0];
    } catch (err) {
      console.log("MESSAGE", err.name);
      throw new AppError(err.message, err.name, err.statusCode || 500);
    }
  },

  delete_todo: async (Model, condition) => {
    try {
      const deleteTodo = await Model.destroy(condition);
      console.log("del", deleteTodo);
      if (!deleteTodo) {
        throw new AppError("Not Found !", "DATANOTFOUND", 404);
      }
      return deleteTodo;
    } catch (err) {
      console.log("MESSAGE", err.name);
      throw new AppError(err.message, err.name, err.statusCode || 500);
    }
  },
};
